De applicatie bestaat uit een spring boot backend waar een h2 database achter zit. Deze database wordt door spring boot geïnitialiseerd door een script op de volgende locatie: `src/main/resources/data.sql`.

De frontend is gemaakt met plain javascript en praat met de backend via een REST API. De source hiervan staat in `src/main/resources/static`.

Wanneer de applicatie wordt gestart is de index pagina te vinden op [http://localhost:8080/index.html]()
