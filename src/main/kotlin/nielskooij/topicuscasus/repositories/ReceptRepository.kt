package nielskooij.topicuscasus.repositories

import nielskooij.topicuscasus.domain.Recept
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface ReceptRepository : CrudRepository<Recept, Long>