package nielskooij.topicuscasus.repositories

import nielskooij.topicuscasus.domain.Medicijn
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface MedicijnRepository : CrudRepository<Medicijn, Long>