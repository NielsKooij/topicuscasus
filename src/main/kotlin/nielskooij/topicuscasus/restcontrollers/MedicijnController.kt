package nielskooij.topicuscasus.restcontrollers

import nielskooij.topicuscasus.domain.Medicijn
import nielskooij.topicuscasus.repositories.MedicijnRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController

@RestController
class MedicijnController(
        @Autowired
        val medicijnRepository: MedicijnRepository
) {

    @GetMapping("/medicijnen")
    fun getMedicijnen() : Iterable<Medicijn> =
        medicijnRepository.findAll()

    @GetMapping("/medicijn/{id}")
    fun getMedicijnById(@PathVariable id: Long): Medicijn {
        val medicijn = medicijnRepository.findById(id)

        if(!medicijn.isPresent) {
            throw RuntimeException("No medicijn with id($id)")
        }

        return medicijn.get()
    }

}