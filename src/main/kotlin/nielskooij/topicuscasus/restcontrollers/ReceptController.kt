package nielskooij.topicuscasus.restcontrollers

import nielskooij.topicuscasus.domain.ReceptMedicijn
import nielskooij.topicuscasus.domain.Recept
import nielskooij.topicuscasus.domain.dto.MedicijnVoorschrift
import nielskooij.topicuscasus.domain.dto.ReceptDto
import nielskooij.topicuscasus.repositories.MedicijnRepository
import nielskooij.topicuscasus.repositories.ReceptRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
class ReceptController(
        @Autowired
        val receptRepository: ReceptRepository,

        @Autowired
        val medicijnRepository: MedicijnRepository
) {

    @PostMapping("/recept/createNew")
    fun createRecept(@RequestBody receptDto: ReceptDto): ReceptDto {
        val recept = Recept(
                id = receptDto.id,
                bsn = receptDto.bsn,
                afleverMethode = receptDto.afleverMethode,
                voorschrijfDatum = receptDto.voorschrijfDatum,
                eindDatum = receptDto.eindDatum,
                medicijnen = receptDto.medicijnen.map {
                    ReceptMedicijn(
                            id = 0,
                            medicijn = medicijnRepository.findById(it.medicijnId).get(),
                            voorschrift = it.voorschrift
                    )
                }
        )

        val updatedRecept = receptRepository.save(recept)
        return ReceptDto(
                id = updatedRecept.id,
                bsn = updatedRecept.bsn,
                afleverMethode = updatedRecept.afleverMethode,
                voorschrijfDatum = updatedRecept.voorschrijfDatum,
                eindDatum = updatedRecept.eindDatum,
                medicijnen = updatedRecept.medicijnen.map {
                    MedicijnVoorschrift(it.medicijn.id, it.voorschrift)
                }
        )
    }
}