package nielskooij.topicuscasus

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class TopicusCasusApplication

fun main(args: Array<String>) {
	runApplication<TopicusCasusApplication>(*args)
}
