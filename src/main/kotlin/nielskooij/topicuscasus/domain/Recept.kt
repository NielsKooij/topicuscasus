package nielskooij.topicuscasus.domain

import org.springframework.format.annotation.DateTimeFormat
import java.util.*
import javax.persistence.*

@Entity
data class Recept(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long,
        var bsn: String,

        @DateTimeFormat(pattern = "yyyy-mm-dd")
        var voorschrijfDatum: Date,

        @DateTimeFormat(pattern = "yyyy-mm-dd")
        var eindDatum: Date,

        var afleverMethode: String,

        @OneToMany(cascade = [CascadeType.ALL])
        var medicijnen: List<ReceptMedicijn>
)