package nielskooij.topicuscasus.domain.dto

import org.springframework.format.annotation.DateTimeFormat
import java.util.*

data class ReceptDto (
        var id: Long,
        var bsn: String,

        @DateTimeFormat(pattern = "yyyy-mm-dd")
        var voorschrijfDatum: Date,

        @DateTimeFormat(pattern = "yyyy-mm-dd")
        var eindDatum: Date,

        var afleverMethode: String,

        var medicijnen: List<MedicijnVoorschrift>
)

data class MedicijnVoorschrift(val medicijnId: Long, val voorschrift: String)