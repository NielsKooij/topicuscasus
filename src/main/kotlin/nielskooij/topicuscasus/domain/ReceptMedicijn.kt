package nielskooij.topicuscasus.domain

import javax.persistence.*

@Entity
data class ReceptMedicijn(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long,

        @OneToOne
        val medicijn: Medicijn,

        val voorschrift: String
)