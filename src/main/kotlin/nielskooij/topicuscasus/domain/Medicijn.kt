package nielskooij.topicuscasus.domain

import javax.persistence.*

@Entity
data class Medicijn(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long,
        val naam: String,
        val vorm: String,

        @OneToOne(cascade = [CascadeType.ALL])
        val verpakkingsGegevens: VerpakkingsGegevens
)