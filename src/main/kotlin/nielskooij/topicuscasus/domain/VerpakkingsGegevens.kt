package nielskooij.topicuscasus.domain

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

@Entity
data class VerpakkingsGegevens (
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long,
        val eenheid: String,
        val aantal: Int?,
        val tablettenPerStrip: Int?,
        val mlPerFlesje: Int?
)