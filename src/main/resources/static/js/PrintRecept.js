function fillRecept(recept) {
    $("#bsn").append(recept.bsn);
    $("#voorschrijfDatum").append(recept.voorschrijfDatum);
    $("#afleverMethode").append(recept.afleverMethode);
    $("#eindDatum").append(recept.eindDatum);

    for(let i = 0; i < recept.medicijnen.length; i++) {
        let medicijn = recept.medicijnen[i];
        getMedicijn(medicijn.medicijnId, medicijn.voorschrift, addMedicijn)
    }
}

function addMedicijn(medicijn, voorschrift) {
    let medicijnDiv =
        "<div class=\"bordered\">" +
            "<p>" + medicijn.naam + " " + medicijn.vorm + "</p>" +
            "<p>Aantal: " + medicijn.verpakkingsGegevens.aantal + "</p>" +
            "<p>Voorschrift: " + voorschrift + "</p>" +
        "</div>";

    $("#medicijnList").append(medicijnDiv);
}

function getMedicijn(medicijnId, voorschrift, callback) {
    $.ajax({
        type: "GET",
        contentType: "application/json",
        url: "http://localhost:8080/medicijn/" + medicijnId,
        dataType: "json",
        success: function(medicijn) {
            callback(medicijn, voorschrift)
        },
        error: function(err) {
            console.log("Error bij ophalen medicijn: ")
            console.log(err)
        }
    });
}

let recept = JSON.parse(localStorage.getItem("currentRecept"));
fillRecept(recept);