function fillForm(recept) {
    $("#inputBsn").val(recept.bsn);
    $("#inputAfleverMethode").val(recept.afleverMethode);

    _.each(recept.medicijnen, medicijn => getMedicijn(medicijn.medicijnId, showMedicijn));
}

function showMedicijn(medicijn) {
    let medicijnDiv =
        "<div class=\"bordered\">" +
            "<p>" + medicijn.naam + " " + medicijn.vorm + "</p>" +
            "<p>Aantal: " + medicijn.verpakkingsGegevens.aantal + "</p>" +
            "<div>" +
                "<label for=\"" + medicijn.id + "\">Voorschrift: </label>" +
                "<input name=\"medicijn\" type=\"text\" id=\"" + medicijn.id + "\">" +
            "</div>" +
        "</div>";

    $("#medicijnList").append(medicijnDiv);
}

function getMedicijn(medicijnId, callback) {
    $.ajax({
        type: "GET",
        contentType: "application/json",
        url: "http://localhost:8080/medicijn/" + medicijnId,
        dataType: "json",
        success: function(medicijn) {
            callback(medicijn)
        },
        error: function(err) {
            console.log("Error bij ophalen medicijn: ")
            console.log(err)
        }
    });
}

function getReceptFromForm() {
    return {
        id: 0,
        bsn: $("#inputBsn").val(),
        voorschrijfDatum: $("#inputVoorschrijfdatum").val(),
        eindDatum: $("#inputEindDatum").val(),
        afleverMethode: $("#inputAfleverMethode").val(),
        medicijnen: _.map($("input[name='medicijn']"), function(input) {
            return {
                medicijnId: input.id,
                voorschrift: input.value
            }
        })
    };
}

function isValidRecept(recept) {
    let isValid =
        recept.id == 0 &&
        recept.bsn && recept.bsn !== "" &&
        recept.voorschrijfDatum &&
        recept.eindDatum &&
        recept.afleverMethode && recept.afleverMethode !== "";

    for(let i = 0; i < recept.medicijnen.length; i++) {
        let medicijn = recept.medicijnen[i];
        isValid = isValid && medicijn.id !== 0 && medicijn.voorschrift && medicijn.voorschrift !== "";
    }
    return isValid
}

$("#createRecept").click(function() {
    let updatedRecept = getReceptFromForm();

    if(isValidRecept(updatedRecept)) {
        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: "http://localhost:8080/recept/createNew",
            dataType: "json",
            data: JSON.stringify(updatedRecept),
            success: function(recept) {
                localStorage.setItem("currentRecept", JSON.stringify(recept))
                window.location.href = "http://localhost:8080/PrintRecept.html";
            },
            error: function(err) {
                console.log("Error bij opslaan recept: ")
                console.log(err)
            }
        });
    } else {
        alert("Nog niet alles is ingevuld");
    }
});

let recept = JSON.parse(localStorage.getItem("currentRecept"));
fillForm(recept)