function showMedicijn(medicijn) {
    let medicijnDiv =
        "<div class=\"bordered container\">" +
            "<div class=\"row\">" +
                "<div class=\"col-xs-6\">" +
                    "<p>Naam: " + medicijn.naam + "</p>" +
                    "<p>Vorm: " + medicijn.vorm + "</p>" +
                    "<p>Aantal: " + medicijn.verpakkingsGegevens.aantal + " " + medicijn.verpakkingsGegevens.eenheid + "</p>";

    if(medicijn.verpakkingsGegevens.tablettenPerStrip) {
        medicijnDiv += "<p>Aantal tabletten per strip: " + medicijn.verpakkingsGegevens.tablettenPerStrip + "</p>";
    }

    if(medicijn.verpakkingsGegevens.mlPerFlesje) {
        medicijnDiv += "<p>Aantal mililiter per flesje: " + medicijn.verpakkingsGegevens.mlPerFlesje + "</p>";
    }

    medicijnDiv +=
                "</div>" +
                "<div class=\"custom-control custom-checkbox col-xs-6\">" +
                    "<input name=\"medicijn\" type=\"checkbox\" class=\"custom-control-input\" id=\"" + medicijn.id + "\">" +
                    "<label class=\"custom-control-label\" for=\"" + medicijn.id + "\">Voeg toe aan recept</label>" +
                "</div>" +
            "</div>" +
        "</div>";

    $("#medicijnList").append(medicijnDiv);
}

function showMedicijnen() {
    $.ajax({
        type: "GET",
        contentType: "application/json",
        url: "http://localhost:8080/medicijnen",
        dataType: "json",
        success: function(medicijnen) {
            _.each(medicijnen, medicijn => showMedicijn(medicijn))
        },
        error: function(err) {
            console.log("Error bij ophalen medicijnen: ")
            console.log(err)
        }
    });
}

$("#createRecept").click(function() {
    let recept = {
        id: 0,
        medicijnen: _.map($("input:checked"), function(input) {
            return {
                medicijnId: input.id
            }
        })
    };

    if(recept.medicijnen.length > 0) {
        localStorage.setItem("currentRecept", JSON.stringify(recept));
        window.location.href = "http://localhost:8080/MaakRecept.html";
    } else {
        alert("Selecteer ten minste één medicijn");
    }
});

showMedicijnen();