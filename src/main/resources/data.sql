insert into verpakkings_gegevens (aantal, eenheid, ml_per_flesje, tabletten_per_strip)
values (3, 'strips', null, 10),
       (1, 'flesje', 50, null),
       (1, 'flesje', 10, null);

insert into medicijn(naam, vorm, verpakkings_gegevens_id)
values ('Paracetamol', 'tablet 500mg', 1),
       ('Broomhexine', 'drank 0,8mg/ml', 2),
       ('Chlooramfenicol', 'oogdruppels 5 mg/ml', 3);